<h1 align="center">Hi 👋, I'm Kacper</h1>

- 🔭 I’m currently working on [Tiny Garden](https://justtinygames.itch.io/tiny-garden)

- 🌱 I’m currently learning **TypeScript**

- 💬 Ask me about **Unreal, Godot, .NET**

<h2> Projects </h2>
<h3>Shop Invoice</h3>
<ul>
    <li>ASP.NET</li>
    <li>.NET Framework</li>
    <li>MySQL</li>
</ul>
<a href="https://gitlab.com/mrdread6/ShopInvoice">Repository</a>
<h3> Sport CMS </h3>
<ul>
    <li>Vue.js</li>
    <li>Spring boot</li>
    <li>Tailwind css</li>
</ul>
<a href="https://gitlab.com/mrdread6/sportcms_frontend">Frontend</a> +
<a href="https://gitlab.com/mrdread6/sportcms_backend">Backend</a>

<h3> Macro Deck Plugin for Elite Dangerous</h3>
<ul>
    <li>.NET</li>
</ul>
<a href="https://gitlab.com/mrdread6/macro-deck-elite-dangerous-plugin"> Repo </a>
<h3 align="left">Stack:</h3>
<p align="left">
    <a href="https://azure.microsoft.com/en-in/" target="_blank" rel="noreferrer">
        <img src="https://www.vectorlogo.zone/logos/microsoft_azure/microsoft_azure-icon.svg"
            alt="azure" width="40" height="40" />
    </a>
    <a href="https://www.gnu.org/software/bash/" target="_blank" rel="noreferrer">
        <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash"
            width="40" height="40" />
    </a>
    <a href="https://www.blender.org/" target="_blank" rel="noreferrer">
        <img src="https://download.blender.org/branding/community/blender_community_badge_white.svg"
            alt="blender" width="40" height="40" />
    </a>
    <a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg"
            alt="cplusplus" width="40" height="40" />
    </a>
    <a href="https://www.w3schools.com/cs/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/csharp/csharp-original.svg"
            alt="csharp" width="40" height="40" />
    </a>
    <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg"
            alt="css3" width="40" height="40" />
    </a>
    <a href="https://www.docker.com/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg"
            alt="docker" width="40" height="40" />
    </a>
    <a href="https://dotnet.microsoft.com/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/dot-net/dot-net-original-wordmark.svg"
            alt="dotnet" width="40" height="40" />
    </a>
    <a href="https://git-scm.com/" target="_blank" rel="noreferrer">
        <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40"
            height="40" />
    </a>
    <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg"
            alt="html5" width="40" height="40" />
    </a>
    <a href="https://gohugo.io/" target="_blank" rel="noreferrer">
        <img src="https://api.iconify.design/logos-hugo.svg" alt="hugo" width="40" height="40" />
    </a>
    <a href="https://www.java.com" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg"
            alt="java" width="40" height="40" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"
        rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg"
            alt="javascript" width="40" height="40" />
    </a>
    <a href="https://www.linux.org/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg"
            alt="linux" width="40" height="40" />
    </a>
    <a href="https://mariadb.org/" target="_blank" rel="noreferrer">
        <img src="https://www.vectorlogo.zone/logos/mariadb/mariadb-icon.svg" alt="mariadb"
            width="40" height="40" />
    </a>
    <a href="https://www.microsoft.com/en-us/sql-server" target="_blank" rel="noreferrer">
        <img src="https://www.svgrepo.com/show/303229/microsoft-sql-server-logo.svg" alt="mssql"
            width="40" height="40" />
    </a>
    <a href="https://www.mysql.com/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg"
            alt="mysql" width="40" height="40" />
    </a>
    <a href="https://www.nginx.com" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nginx/nginx-original.svg"
            alt="nginx" width="40" height="40" />
    </a>
    <a href="https://www.postgresql.org" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg"
            alt="postgresql" width="40" height="40" />
    </a>
    <a href="https://www.qt.io/" target="_blank" rel="noreferrer">
        <img src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Qt_logo_2016.svg" alt="qt"
            width="40" height="40" />
    </a>
    <a href="https://sass-lang.com" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sass/sass-original.svg"
            alt="sass" width="40" height="40" />
    </a>
    <a href="https://spring.io/" target="_blank" rel="noreferrer">
        <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring"
            width="40" height="40" />
    </a>
    <a href="https://www.sqlite.org/" target="_blank" rel="noreferrer">
        <img src="https://www.vectorlogo.zone/logos/sqlite/sqlite-icon.svg" alt="sqlite" width="40"
            height="40" />
    </a>
    <a href="https://tailwindcss.com/" target="_blank" rel="noreferrer">
        <img src="https://www.vectorlogo.zone/logos/tailwindcss/tailwindcss-icon.svg" alt="tailwind"
            width="40" height="40" />
    </a>
    <a href="https://vuejs.org/" target="_blank" rel="noreferrer">
        <img
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg"
            alt="vuejs" width="40" height="40" />
    </a>
    <a href="https://godotengine.org/" target="_blank" rel="noreferrer">
        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6a/Godot_icon.svg" alt="godot"
            width="40" height="40" />
    </a>
</p>